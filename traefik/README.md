# Guida per l'utilizzo di traefik


## da mettere in ogni docker compose:

```
networks:
  traefik_network:
    external: true
```
   
## crea variabili di ambiente (in file .env oppure passali nel campo environment)

```
DOMAINNAME=xxx.duckdns.org
DUCKDNS_TOKEN=token-di-duckdns
DUCKDNS_EMAIL=mail-di-duck-dns
TRAEFIK=/path/folder/volume/traefik/
PUID=1000
PGID=1000
TZ=Europe/Rome
```

## crea file

- in cartella indicata in $TRAEFIK crea sotto cartella acme
- fai touch di $TRAEFIK/acme/acme.json e setta permessi mi pare a 600 (in caso si lamenta nei docker log)
- fai touch di $TRAEFIK/traefik.log


## test letsenctypt 
In automatico traefik ottiene i cerificati ssl da letsencrypt

- se ci sono errori (dopo 3) letsencrypt banna l'ip per un certo tempo. Per fare le prove scommentare la riga sotto il commento
```
  # USE THIS WHEN TRYING TO SETUP FIRST TIME WITH STAGING SERVER (no ban)
```
- per fare prove sul server di prova senza rischio di ban

- **NB**: di default è presente una regola che fa redirect tutti gli http in https


            
## come aggiungere un servizio a traefik

dato un qualsiasi docker compose di un qualsiasi servizio che per esempio espone su IP porta 1234, allora aggiungi a quel service

**NB in file .env è definito DOMAINNAME=nomedominio.it**

```
services:
	service_name:
	    image: image_repo/image_name
	    ## QUESTO SOTTO VA AGGIUNTO ##
	    labels:
          - "traefik.enable=true"
          - "traefik.docker.network=traefik_network"
          ## QUESTO PER AVERE INDIRIZZO SOLO LOCALE (devi registrare nome.lan su pihole o nel file di host) ######
          ## HTTP Routers
          - "traefik.http.routers.nome_univoco_local.entrypoints=https"
          - "traefik.http.routers.nome_univoco_local.rule=Host(`nome_dns.lan`)"
          - "traefik.http.routers.nome_univoco_local.service=nome_univoco_servizio"
          - "traefik.http.services.nome_univoco_servizio.loadbalancer.server.port=1234" # questa è la porta che servira traefik
          - "traefik.http.routers.nome_univoco_local.tls=true"
          ## REMOTE #########################################################################
          ## HTTP Routers
          - "traefik.http.routers.nome_univoco_remote.entrypoints=https"
          - "traefik.http.routers.nome_univoco_remote.rule=Host(`nome_dns.$DOMAINNAME`)"
          ## HTTP Services
          - "traefik.http.routers.nome_univoco_remote.service=nome_univoco_servizio_remoto"
          - "traefik.http.services.nome_univoco_servizio_loadbalancer.remoto.server.port=1234" # questa è la porta che servira traefik
          - "traefik.http.routers.nome_univoco_remote.tls=true"
      networks:
      - traefik_network # QUESTO serve per parlare con traefik
      - eventuali_altri_network_per_parlare_con_altri_servizi

```  
		




