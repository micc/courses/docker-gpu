# Istruzioni installazione nvidia-docker

https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html


## Esempi di sviluppo GPU

**Prima di eseguire i comandi di build/up ecc..**
- Copiare il file pytorch/.env.example in pytorch/.env riempiendo le variabili di ambiente

1. pytorch: crea una immagine di docker in cui l'utente è root (**FORTEMENTE SCONSIGLIATO**) (*comandi make pytorch-XXX*)
2. pytorch-user: crea una immagine di docker in cui l'utente è micc-docker con ID passato al build del sistema  (*comandi make pytorch-user-XXX*)
3. pytorch-ssh: crea una immagine di docker accessibile via ssh in cui è possibile usare sia root (entrando via make pytorch-ssh-shell) sia micc-docker (collegandosi via ssh) (*comandi make pytorch-ssh-XXX*)