SHELL := /bin/bash
.SILENT:
.PHONY: help
.DEFAULT_GOAL := help

SOME_FOLDER := /path/to/folder
DOCKER_CONTAINER_NAME := container_name
DOCKER_IMAGE_NAME := docker_image_name

help:  ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)


##@ run code using docker or nvidia docker (not compose)

run-docker:	## run docker
	@echo "Run container with custom command (i.e. interactive bash)"
	docker run --gpus all --rm \
		-v `pwd`/src:/code \
		-v $(SOME_FOLDER):/some_folder_on_container \
		--name $(DOCKER_CONTAINER_NAME) \
		-it $(DOCKER_IMAGE_DEV) /bin/bash

run-legacy-nvidia-docker: ## run docker using legacy nvidia-docker (used before nvidia-docker2)
	@echo "Run container using legacy nvidia docker"
	nvidia-docker run --rm \
		-v `pwd`/src:/code \
		--name $(DOCKER_CONTAINER_NAME) \
		-it $(DOCKER_IMAGE_DEV) /bin/bash


##@ commands for pytorch build with ROOT access

pytorch-build:  ## Build development docker
	docker-compose -p "pytorch-develop" -f pytorch/docker-compose.yml build

pytorch-stop:	## stop the containers
	@echo "Stopping running container"
	docker-compose -p "pytorch-develop" -f pytorch/docker-compose.yml stop

pytorch-down:   ## stop containers and removes the stopped containers as well as any networks that were created.
	docker-compose -p "pytorch-develop" -f pytorch/docker-compose.yml down --remove-orphans

pytorch-run:   ## stop containers and removes the stopped containers as well as any networks that were created.
	docker-compose -p "pytorch-develop" -f pytorch/docker-compose.yml up -d

pytorch-logs:   ## show logs of name_of_container
	docker-compose -p "pytorch-develop" -f pytorch/docker-compose.yml logs pytorch

pytorch-shell:	## launch command on running container
	docker-compose -p "pytorch-develop" -f pytorch/docker-compose.yml exec pytorch /bin/bash

pytorch-detect:	## launch command on running container
	docker-compose -p "pytorch-develop" -f pytorch/docker-compose.yml exec pytorch python3 /code/detect.py \
	--weights /input/weights/yolov5s.pt --source /input/images --project /output


##@ run utility for pytorch build with USER access

pytorch-user-build:  ## Build development docker
	docker-compose -p "pytorch-user-develop" -f pytorch/docker-compose-user.yml build

pytorch-user-stop:	## stop the containers
	@echo "Stopping running container"
	docker-compose -p "pytorch-user-develop" -f pytorch/docker-compose-user.yml stop

pytorch-user-down:   ## stop containers and removes the stopped containers as well as any networks that were created.
	docker-compose -p "pytorch-user-develop" -f pytorch/docker-compose-user.yml down --remove-orphans

pytorch-user-run:   ## stop containers and removes the stopped containers as well as any networks that were created.
	docker-compose -p "pytorch-user-develop" -f pytorch/docker-compose-user.yml up -d

pytorch-user-logs:   ## show logs of name_of_container
	docker-compose -p "pytorch-user-develop" -f pytorch/docker-compose-user.yml logs pytorch-user

pytorch-user-shell:	## launch command on running container
	docker-compose -p "pytorch-user-develop" -f pytorch/docker-compose-user.yml exec pytorch-user /bin/bash

pytorch-user-detect:	## launch command on running container
	docker-compose -p "pytorch-user-develop" -f pytorch/docker-compose-user.yml exec pytorch-user python3 /code/detect.py \
	--weights /input/weights/yolov5s.pt --source /input/images --project /output


##@ commands for Pytorch with SSH access

pytorch-ssh-build:  ## Build development docker
	docker-compose -p "pytorch-develop-ssh" -f pytorch/docker-compose-ssh.yml build

pytorch-ssh-stop:	## stop the containers
	@echo "Stopping running container"
	docker-compose -p "pytorch-develop-ssh" -f pytorch/docker-compose-ssh.yml stop

pytorch-ssh-down:   ## stop containers and removes the stopped containers as well as any networks that were created.
	docker-compose -p "pytorch-develop-ssh" -f pytorch/docker-compose-ssh.yml down --remove-orphans

pytorch-ssh-run:   ## stop containers and removes the stopped containers as well as any networks that were created.
	docker-compose -p "pytorch-develop-ssh" -f pytorch/docker-compose-ssh.yml up -d

pytorch-ssh-logs:   ## show logs of name_of_container
	docker-compose -p "pytorch-develop-ssh" -f pytorch/docker-compose-ssh.yml logs pytorch-ssh

pytorch-ssh-shell:	## launch command on running container
	docker-compose -p "pytorch-develop-ssh" -f pytorch/docker-compose-ssh.yml exec pytorch-ssh /bin/bash


##@ General Docker UTILS

docker-clean: ## remove all dangling images, containers
	docker system prune


# Commands examples: uncomment and edit

# ##@ group descritpion
#
# up:	## Start development
# 	docker-compose -p "project_name" -f docker-compose.yaml up -d
#
# build:  ## Build development docker
# 	docker-compose -p "project_name" -f docker-compose.yaml build
#
# stop:	## stop the containers
# 	@echo "Stopping running container"
# 	docker-compose -p "project_name" -f docker-compose.yaml stop
#
# down:   ## stop containers and removes the stopped containers as well as any networks that were created.
# 	docker-compose -p "project_name" -f docker-compose.yaml down
#
# logs:   ## show logs of name_of_container
# 	docker-compose -p "project_name" -f docker-compose.yaml logs name_of_container
#
# shell:	## launch command on running container
# 	docker-compose -p "project_name" -f docker-compose.yaml exec name_of_container sh
#
# restart:	down up